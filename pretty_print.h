#include <string.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct String_View
{
    size_t loc;
    char *data;
    size_t size;
} String_View_t;

String_View_t *string_view_new(size_t size)
{
    String_View_t *new_string = (String_View_t*) malloc(sizeof(String_View_t));
    new_string->data = (char* ) malloc(size);
    new_string->size = size;
    new_string->loc = 0;
    return new_string;
}

String_View_t *string_view_append_char(String_View_t *old, char c)
{
    size_t len = old->loc + 1;
    String_View_t *new_string = string_view_new(len);
    new_string->data = old->data;
    memcpy(new_string->data + 1, &c, sizeof(c));
    new_string->size += 1;
    return new_string;
}
