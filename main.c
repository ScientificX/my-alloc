// TODO: Implemet chunk_list_insert keeping in mind that the input list is sorted done
// TODO: Implement chunk_list_remove 'done'
// TODO: Implement chunk_list_find '' done
// TODO: Implement chunks_coalesing
// TODO: Implement free chunk reclamation when allocating 
// TODO: Implement splitting data if the free chunk is not exactly the same size as requested allocation

#include <stdio.h>
#include <stdbool.h>
#include <assert.h>
#include <stdlib.h>
#include "pretty_print.h"

#define HEAP_CAPACITY 640000
#define HEAP_CHUNK_CAPACITY 1024
#define MAX_DELETE_COUNT 100

typedef struct
{
    void *start;
    size_t size;
    bool reachable;
} Heap_Chunk;

typedef struct
{
    size_t deleted;
    size_t count;
    Heap_Chunk chunks[HEAP_CHUNK_CAPACITY];
} Chunk_List;

void chunk_dump(Heap_Chunk *heap_chunk)
{
    if (heap_chunk->reachable)
    {
        printf("start: %p, size: %zu \n", heap_chunk->start, (heap_chunk->size));
    }
}

void chunk_list_dump(Chunk_List *list)
{
    printf("Allocated chunks: %zu\n", list->count);
    for (int i = 0; i < list->count; i++)
    {
        chunk_dump(&list->chunks[i]);
    }
}

// I need to change this thing to binary search
Heap_Chunk *chunk_list_find(Chunk_List *list, void *ptr)
{
    for (int i = 0; i < list->count; i++)
    {
        Heap_Chunk curr = list->chunks[i];
        if (curr.reachable && curr.start == ptr)
        {
            return &list->chunks[i];
        }
    }
    return 0;
}

void chunk_list_insert(Chunk_List *list, void *ptr, size_t size)
{
    assert(list->count + 1 <= HEAP_CHUNK_CAPACITY);
    Heap_Chunk *new_chunk = (Heap_Chunk *)malloc(sizeof(Heap_Chunk));
    new_chunk->size = size;
    new_chunk->start = ptr;
    size_t i = list->count - 1;
    while (list->chunks[i].start > ptr && (i >= 1))
    {
        list->chunks[i + 1] = list->chunks[i];
        i -= 1;
    }
    if (i == 0)
    {
        // Heap_Chunk temp = list->chunks[1];
        list->chunks[1] = list->chunks[0];
        list->chunks[0] = *new_chunk;
    }
    else
    {

        list->chunks[i + 1] = *new_chunk;
    }
    printf("%zu", i);
    list->count += 1;
}

void chunk_list_coalese(Chunk_List *list){

}

void chunk_list_remove(Chunk_List *list, void *ptr, size_t index)
{
    if (list->deleted > MAX_DELETE_COUNT) {
        // chunk_list_coalese();
    }
    for (int i = 0; i < list->count; i++)
    {
        Heap_Chunk curr = list->chunks[i];
        if (curr.reachable && curr.start == ptr)
        {
            curr.reachable = false;
            list->count -= 1;
            list->deleted += 1;
            return;
        }
    }
}

char heap[HEAP_CAPACITY] = {0};
static size_t heap_position = 0;

Chunk_List heap_chunks = {0};
Chunk_List freed_chunks = {0};

void *heap_alloc(size_t size)
{
    if (size > 0)
    {
        assert(heap_position + size <= HEAP_CAPACITY);
        // First move the heap to cover the entire size of the previously allocated memeory
        void *pos = heap + heap_position;
        // increment the currently used up space in teh heap
        heap_position += size;

        // add the newly created chunk to the metadata tracking structure
        const Heap_Chunk chunk = {
            .size = size,
            .start = pos,
            .reachable = true,
        };

        heap_chunks.chunks[heap_chunks.count] = chunk;
        heap_chunks.count += 1;

        return pos;
    }
    else
    {
        return NULL;
    }
}

void heap_free(void *mem)
{
    void *mel = mem;
    assert(false && "TODO: HEAP FREEE NOT IMPLEMENTED YET");
}

void get_free_chunks()
{
    assert(false && "TODO: GET FREE CHUNS NOT IMPLEMENTED YET");
}

int main()
{

    for (int i = 0; i < 100; i++)
    {
        // heap_alloc(i);
    }
    heap_alloc(1);
    heap_alloc(2);
    heap_alloc(3);

    chunk_list_dump(&heap_chunks);
    printf("\n");
    // void* ptr = heap_alloc(201);

    chunk_list_insert(&heap_chunks, 1234987, 9934);
    chunk_list_dump(&heap_chunks);

    // chunk_list_dump(&heap_chunks);

    // char *mem = heap_alloc(69);
    // for (int i =0; i< 26; i++){
    //     mem[i] = 'A' + i;
    //     char* buf[26] = {0};
    //     memcpy(buf, mem, 26);
    //     printf("Here: %s \n", (buf));
    // }
    // heap_free(mem);
    // String_View_t *first = string_view_new(10);
    // String_View_t *second = string_view_new(10);
    // char* str = "otiger";
    // memcpy(first->data, str,6);
    // char a = 'a';
    // String_View_t* result = string_view_append_char(first, a);
    return 0x01234;
}